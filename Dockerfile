FROM python:latest
RUN mkdir /api
WORKDIR /api
COPY . /api/
RUN apt update
RUN apt install -y apt-transport-https postgresql libpq-dev libkeyutils-dev
RUN pip3 install -r requirements.txt